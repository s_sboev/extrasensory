<?php
	class Extrasensory				//Класс, описывающий экстрасенса
	{
		private $name;							//Имя экстрасенса
		private $answers;						//список его ответов
		private $userNumbers;					//список введенных пользователем чисел
		private $reliability;					//уровень достоверности экстрасенса
		public function __construct($_name)     //конструктор класса (принимает имя экстрасенса, остальные переменные зануляются)
		{
			$this->name = $_name;
			$this->answers = array();
			$this->userNumbers = array();
			$this->reliability = 0;
		}
		
		public function predictAnswer()				//функция "Предсказывает" загаданное число
		{
			$newAnswer = mt_rand(10,99);			//Получаем случайное число
			$this->answers[] = $newAnswer;			//сохраняем число в перечень ответов
			return $newAnswer;						//возвращаем число
		}
		
		public function validateAnswer($userNumber)	    //проверка правильности предсказанного числа (принимает введенное пользователем число)
		{
			$this->userNumbers[] = $userNumber;			//сохраняем пользовательское число в массив введенны пользователем чисел
			if(end($this->answers) == $userNumber){ //если предсказанное число совпало с пользовательским
				$this->reliability++;				//увеличить уровень достоверности
			}
			else{									//иначе
				$this->reliability--;				//уменьшить уровень достоверности
			}
		}
		
		public function getName()					//функция получения имени экстрасенса
		{
			return $this->name;
		}
		
		public function getReliability()			//функция получения уровня достоверности экстрасенса
		{
			return $this->reliability;
		}
		
		public function getAnswers()			//функция получения ответов экстрасенса
		{
			return $this->answers;
		}
		
		public function getUserNumbers()			//функция получения Введенных пользователем чисеел, которые пытался угадать экстрасенс
		{
			return $this->userNumbers;
		}
		
		public function getObject()				//Функция получения всех свойств в виде объекта
		{
			$dynamics = end($this->answers) == end($this->userNumbers);
			return [
				'name'=>$this->name, 
				'reliability'=>$this->reliability, 
				'answers'=>$this->answers, 
				'userNumbers'=>$this->userNumbers,
				'dynamics'=>$dynamics
			];
		}
	}
?>
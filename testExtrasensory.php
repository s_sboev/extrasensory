<?php 
	require_once("Extrasensory.php");			//подключить класс Extrasensory
	executeAction($_POST['action'],$_POST['parameter']);
	function executeAction($action, $parameter) //функция-меню (вызывает указанную функцию $action с параметром $parameter)
	{
		switch($action){
			case "predict":
				predictNumbers();
				break;
			case "validate":
				validateNumbers($parameter["userNumber"]);
				break;
			case "call":
				callExtrasensory($parameter["countExtrasensory"]);
				break;
			case "showNumbers":
				showNumbers();
				break;
		}
	}
	
	function predictNumbers()			//Функция предсказания чисел экстрасенсами
	{
		$predictResults = array();		//массив для объектов вида {Имя экстрасенса, предсказанное число}
		session_start();									//старт сессии
		if(isset($_SESSION['extrasensories'])){					//если в сессии сохранены экстрасенсы
			$extrasensories = $_SESSION['extrasensories'];  		//сохранить их в массив
			foreach($extrasensories as $extrasensory){							//для каждого экстрасенса
				$result["name"] = $extrasensory->getName();						//получаем его имя
				$result["predictValue"] = $extrasensory->predictAnswer();		//просим его угадать число
				$predictResults[] = $result;									//сохраняем информацию в массив
			}
		}
		echo json_encode($predictResults);									//возвращаем массив в Ajax в формате json
	}
	
	function validateNumbers($userNumber)			//Функция проверки предсказания чисел экстрасенсами
	{
		$extrasensoryStats = array();							//массив для сбора статистики по экстрасенсам
		session_start();									//старт сессии
		if(isset($_SESSION['userNumbers'])){
			$userNumdersAll = $_SESSION['userNumbers'];
		}
		else{
			$userNumdersAll = array();
		}
		$userNumdersAll[]=$userNumber;
		$_SESSION['userNumbers'] = $userNumdersAll;
		if(isset($_SESSION['extrasensories'])){					//если в сессии сохранены экстрасенсы
			$extrasensories = $_SESSION['extrasensories'];  		//сохранить их в массив
			foreach($extrasensories as $extrasensory){				//для каждого экстрасенса
				$extrasensory->validateAnswer($userNumber);     	//Проверить правильность предсказания и изменить уровень достоверности
				$extrasensoryStats[] = $extrasensory->getObject();  //Сохраняем обновленную информацию по экстрасенсу в массив
			}
		}
		echo json_encode($extrasensoryStats);					//возвращаем массив в Ajax в формате json
	}
	
	function callExtrasensory($count)			//Функция создает (зовет на тестирование) $count новых экстрасенсов
	{
		session_start();									//старт сессии
		if(isset($_SESSION['extrasensories']))				//если в сессии сохранены экстрасенсы
			$extrasensories = $_SESSION['extrasensories'];  //сохранить их в массив
		else{												//иначе
			$extrasensories = array();						//Создать пустой массив экстрасенсов
		}
		$countCur =count($extrasensories);		//Текущее количество экстрасенсок
		for($i=0; $i<$count; $i++){				//Выполнить $count раз
			$name = "Экстрасенс ".$countCur;			//Создать экстрасенсу имя
			$extrasensory = new Extrasensory($name);	//Создать нового экстрасенса
			$extrasensories[] = $extrasensory;			//Добавить экстрасенса в массив
			$countCur++;								//Увеличить текущее количество экстрасенсов
		}
		$_SESSION['extrasensories'] = $extrasensories;  //сохранить новый массив экстрасенсов в сессию
		echo $countCur;							//вернуть количество экстрасенсов
	}
	
	function showNumbers()								//Функция для показа введенных пользователем чисел
	{
		session_start();
		if(isset($_SESSION['userNumbers'])){
			$userNumdersAll = $_SESSION['userNumbers'];
		}
		else{
			$userNumdersAll = array();
		}
		echo json_encode($userNumdersAll);
	}
?>
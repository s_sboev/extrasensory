<?php 
	session_start();
	if(isset($_SESSION['extrasensories']))
		$extrasensories = $_SESSION['extrasensories'];
	else{
		$extrasensories = array();
		$_SESSION['extrasensories'] = $extrasensories;
	}
	if(isset($_SESSION['userNumbers'])){
		$userNumdersAll = $_SESSION['userNumbers'];
	}
	else{
		$userNumdersAll = array();
	}
	$countExtrasensories = count($extrasensories);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Тестирование экстрасенсов</title>
		<link rel="stylesheet" type="text/css" href="testExtrasensory.css" >
		<script src="jquery-3.3.1.min.js"></script>
		<script src="testExtrasensory.js"></script>
	</head>
	<body>
		<div class="header">
			<h1>Добрый день. Вы можете протестировать наших экстрасенсов</h1>
		</div>
		<div class="main-block" id="no-extrasensories" <?php if($countExtrasensories >= 2) echo "style='display:none;'"?>>
			<h3>Экстрасенсы еще не пришли. Вы можете позвать нескольких.</h3>
			<p>Для верного тестирования необходимо 2 и более экстрасенсов. Для Вашего удобства рекомендуем тестировать не более 10 экстрасенсов одновременно.</p>
			<div>Позвать <input type="number" id="extrasensory-count" min="2"> экстрасенсов <button id="call-extrasensory-start" onclick="callExtrasensories()">Позвать</button></div>
		</div>
		<div class="main-block" id="make-number" <?php if($countExtrasensories < 2) echo "style='display:none;'"?>>
			<h3>Экстрасенсы готовы к тестированию</h3>
			<div id="printed-numbers">
			<?php if($userNumdersAll):?>
				<p>Вы уже вводили числа:
					<?php foreach($userNumdersAll as $number) echo $number.' ' ?>
				</p>
			<?php endif ?>
			</div>
			<div>Загадайте двухзначное число. Когда будете готовы, нажмите <button onclick="predictValues()">на кнопку</button></div>
		</div>
		<div class="main-block" id="results" style='display:none;'>
			<h3>Варианты от экстрасенсов:</h3>
			<table class="main-table" border="1" id="extrasensory-results">
				<thead>
					<tr>
						<th>Имя экстрасенса</th>
						<th>Ответ</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<div>Введите загаданное число <input type="number" id="user-number" min="10" max="99" > 
			<button onclick="validateNumber()">Отправить</button></div>
		</div>
		<div class="main-block" id="statistics" style='display:none;'>
			<h3>Ваш ответ получен. Статистика по экстрасенсам:</h3>
			<div id="extrasensory-stats">
				
			</div>
			<button class="try-again-button" onclick="tryAgain()">Попробовать еще раз</button>
		</div>
		<div class="footer"><h2>Тестовое задание выполнено Сбоевым С.В. 2018г.<h2></div>
	</body>
<html>
function callExtrasensories()
{
	if($("#extrasensory-count").val() < 2){
		alert("Нельзя вызвать менее двух экстрасенсов");
	}
	else{
		$.ajax({
			'type':'post',
			'url':'testExtrasensory.php',
			'data':{
				'action':'call',
				'parameter':{
					'countExtrasensory':$("#extrasensory-count").val()
				}
			},
			'success':function(result){
				$("#no-extrasensories").css('display','none');
				$("#make-number").css('display','block');
				alert("Вызвано "+result+" экстрасенсов");
			}
		});
	}
}

function predictValues()
{
	$.ajax({
		'type':'post',
		'url':'testExtrasensory.php',
		'data':{
			'action':'predict',
			'parameter':""
		},
		'success':function(result){
			$("#make-number").css('display','none');
			$("#results").css('display','block');
			let predicts = $.parseJSON(result);
			let count = predicts.length;
			$("#extrasensory-results tbody").html("");
			for(i=0; i<count; i++){
				$("#extrasensory-results tbody").append("<tr><td>"+predicts[i].name+"</td><td>"+predicts[i].predictValue+"</td></tr>");
				
			}
		}
	});
}

function validateNumber()
{
	if($("#user-number").val() < 10 || $("#user-number").val() > 99){
		alert("Число должно быть двухзначным");
	}
	else{
		$.ajax({
			'type':'post',
			'url':'testExtrasensory.php',
			'data':{
				'action':'validate',
				'parameter':{
					'userNumber':$("#user-number").val()
				}
			},
			'success':function(result){
				$("#user-number").val("")
				$("#results").css('display','none');
				$("#statistics").css('display','block');
				let extrasensoryStats = $.parseJSON(result);
				let count = extrasensoryStats.length;
				$("#extrasensory-stats").html("");
				for(i=0;i<count;i++){
					let statsBlock = '<div class="extrasensory-stats-block">';
					statsBlock+= '<h4>'+extrasensoryStats[i].name+'. Уровень достоверности: '
						+extrasensoryStats[i].reliability+(extrasensoryStats[i].dynamics?'&#8593;':'&#8595;')+'</h4>';
					statsBlock+='<div class="extrasensory-predicts-all">';
					statsBlock+='<div class="header-cell">Предсказанные</div>';
					predictCount = extrasensoryStats[i].answers.length;
					for(j=0; j<predictCount; j++){
						statsBlock+='<div class="cell">'+extrasensoryStats[i].answers[j]+'</div>'
					}
					statsBlock+='</div>';
					statsBlock+='<div class="user-numbers-all">';
					statsBlock+='<div class="header-cell">Загаданные</div>';
					userNumbersCount = extrasensoryStats[i].userNumbers.length;
					for(j=0; j<userNumbersCount; j++){
						statsBlock+='<div class="cell">'+extrasensoryStats[i].userNumbers[j]+'</div>'
					}
					statsBlock+='</div></div>';
					$("#extrasensory-stats").append(statsBlock);
				}
			}
		});
	}
}

function tryAgain()
{
	$.ajax({
		'type':'post',
			'url':'testExtrasensory.php',
			'data':{
				'action':'showNumbers',
				'parameter':""
			},
			'success':function(result){
				
				$("#statistics").css('display','none');
				$("#make-number").css('display','block');
				var userNumbers = $.parseJSON(result);
				var userNumbersCount = userNumbers.length;
				if(userNumbersCount){
					var userNumbersContent = '<p>Вы уже вводили числа: ';
					for(i=0;i<userNumbersCount;i++)
						userNumbersContent += userNumbers[i]+' ';
					userNumbersContent += '</p>';
					$("#printed-numbers").html(userNumbersContent);
				}
			}
	});
	
	
}